VERSION 5.00
Object = "{18576B0E-A129-4A50-9930-59E18A6FE5E1}#1.0#0"; "TeComCanvas.dll"
Begin VB.Form GeoSanComercial 
   Caption         =   "Mapa"
   ClientHeight    =   5475
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   8100
   LinkTopic       =   "Form1"
   ScaleHeight     =   5475
   ScaleWidth      =   8100
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Pan 
      Caption         =   "Move"
      Height          =   495
      Left            =   6120
      TabIndex        =   1
      Top             =   480
      Width           =   1815
   End
   Begin TECOMCANVASLibCtl.TeCanvas TeCanvas1 
      Height          =   4815
      Left            =   240
      OleObjectBlob   =   "Principal.frx":0000
      TabIndex        =   0
      Top             =   360
      Width           =   5655
   End
End
Attribute VB_Name = "GeoSanComercial"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim conexao As New ADODB.connection             'cria uma vari�vel de conex�o ao banco de dados


'Formul�rio vai carregar e ser apresentado para o usu�rio
'
Private Sub Form_Load()
    conexao.connectionString = "Provider=SQLOLEDB.1;Password=Nexus243;Persist Security Info=True;User ID=sa;Initial Catalog=sama;Data Source=192.168.1.20"          'define a string de conex�o com o banco de dados e associa a mesma ao ADODB
    conexao.Open                                'abre a conex�o com o banco de dados
    TeCanvas1.provider = 1                      'fala para o canvas (�rea do mapa), que o banco de dados � o SQLServer
    TeCanvas1.user = "sa"                       'informa ao canvas que o usu�rio � o sa
    TeCanvas1.connection = conexao              'passa a conex�o ADODB ao banco de dados para o canvas
    TeCanvas1.plotView                          'mostra o mapa na tela
End Sub

'Usu�rio selecionou para fechar o formul�rio e sair da aplica��o
'
Private Sub Form_Unload(Cancel As Integer)
    TeCanvas1.closeConnection                   'fecha a conx�o com o canvas
    conexao.Close                               'fecha a conex�o com o banco de dados
    Set conexao = Nothing                       'informa que a conex�o � nula
End Sub

Private Sub Pan_Click()
    TeCanvas1.Pan
End Sub
